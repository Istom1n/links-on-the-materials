# Materials

## Change Log

### 03.11.2018:
- Добавил ссылку курсов по inDesign и Revit (хотя второе просто под руку попалось)

### 27.10.2018:
- Добавил Change Log (*логгирование изменений* т.е. тут я буду писать что я изменил и какого числа. чтобы каждый раз не сообщать об этом)
- Добавил курсы по Cinema 4D
- Добавил курсы по Adobe Photoshop
- Дополнил раздел Adobe Illustrator парой курсов
- Немного поменять разметку, т.к. материала становится больше, чтобы не запутаться
- Нашел и добавил для себя лично пару курсов, так что не удивляйся, если что-то совсем неясное будет для тебя тут
- Добавил TODO чтобы не забыл разобрать что-то еще


## E-commerce UX researches

 ### Nielsen  _Norman_  Group — Ecommerce User Experience (includes 13 UX reports)

![nngroup](https://media.nngroup.com/media/report_images/EcommerceUserExperience_mult.jpg.150x235_q95_autocrop_upscale.jpg)
- [Official Website](https://www.nngroup.com/reports/ecommerce-user-experience/)
- [Read or Download](https://mega.nz/#F!FHplnCbb!Eybtq5uxjLRMjn9XJ1lspA)

---

### Researching the best ways to improve the online user experience - Baymard Institute (includes 6 UX studies)


![e-commerce usability](https://spee.ch/0eecabad59adc2edee9e4b3350a46721ed928c30/baymard.png)
- [Official Website](https://baymard.com/)
- [Read or Download](https://mega.nz/#F!hKwjCQLK!t9mjOERpi4J71wk4tTAOHw)

> **UI** — User Interface (т.е. *пользовательский интерфейс*)
> **UX** — User eXperience (дословно это — *пользовательский опыт*, имеется ввиду, ты человек привык на айфоне нажимать два раза на  кнопку Home чтобы выйти в мультизадачность, а на некоторых Android телефонах такого нет, получается опыт того как открыть у пользователя есть, но на Android он не сработает, поэтому надо стараться проектировать так, чтобы на каждом устройстве оно работало интуитивно понятно)


## Medicine

![Erowid](https://i.postimg.cc/3xSGk2Lf/Erowid_2018-09-19_21-21-45.jpg)
- [Erowid](https://www.erowid.org/) — documenting the complex relationship between human & psychoactives;

- [Erowid Chemistry](https://erowid.org/archive/rhodium/chemistry/) — exciting DB [database] of chemical prescriptions of different drugs;

- [PsychonautWiki](https://psychonautwiki.org/wiki/Main_Page) — is a community-driven online encyclopedia that is striving to formally document the field of psychonautics in a comprehensive and scientifically-grounded manner without overly depending on metaphors, analogies, or anecdotal reports;

- [PubMed - NCBI](https://www.ncbi.nlm.nih.gov/pubmed/) — US National Library of Medicine. National Institutes of Health.


## English

- [Oxford Learner's Dictionaries](https://www.oxfordlearnersdictionaries.com/) — definitions, translations, and grammar explanations.

- [Urdan Dictionary](https://www.urbandictionary.com/) — is another way of saying or describing that some one is in need of a tone check, a bitch slap or just a reality check;

- [Thesaurus](https://www.thesaurus.com) — Synonyms and Antonyms of Words.

- [Test your vocab](http://testyourvocab.com/) — How many words do you know?

- [Forvo](https://forvo.com/) — the pronunciation dictionary (*т.е. словарь произношений, самых разных*).
- [Grammarly](https://www.grammarly.com/1) — makes sure everything you type is  
easy to read, effective, and mistake-free (*т.е. проверяет грамматику, построение и т.д.*).

- [News In Levels](https://www.newsinlevels.com/) — i.e. this is the real news for 3 speakers levels.

- [The Punctuation Guide](http://www.thepunctuationguide.com/) — guide for punctuation :)

- [Lang-8](http://lang-8.com/) — service where you can write e.g. essay, native speaker check it and vise versa.

- [typing.com](https://www.typing.com/) — very hard way, but effective. Just try.


## Courses & Tutorials

### Adobe Illustrator

- **Основы Adobe Illustrator**
[Скачать](https://drive.google.com/drive/folders/1nYyMcAS5fErOfcTnyDGLeq-Ts99oOXhM?usp=sharing)

- **[Школа векторной графики Pixel] Курс по рисованию векторных иллюстраций**
[Скачать](https://cloud.mail.ru/public/HHb7/ALcgt6jbc)

- **[BangBangEducation] Cоздание пространства в Adobe Illustrator – Инструменты и Приемы**
[Скачать](https://cloud.mail.ru/public/FX74/Lxsfj21Y1)

- **Супер Illustrator**
[Скачать](https://cloud.mail.ru/public/Eigb/tywZQpg2e)

- **[Специалист.ру] Adobe Illustrator CC CS6 для MAC и PC. Уровень 2. Углубленные возможности (2018)**
[Официальный сайт](https://www.specialist.ru/course/il2)
[Скачать](https://cloud.mail.ru/public/KLBJ/hqw8zeRKg)

---

### Luminar 2018
- [Official Luminar 2018 Video Tutorials](https://skylum.com/video-galleries/luminar-video-tutorials)

- [Luminar Tutorials - YouTube (very not bad basic courses)](https://www.youtube.com/watch?v=62-0oD_v94M&list=PLTShOn29ZxjNsnCLHGBTLUgClcjveZZ1m)

- [Compare the Luminar & Lightroom Advantages](https://skylum.com/luminar/compare)

---

### Adobe Lightroom

- **Базовый курс по Adobe Lightroom**
[Скачать](https://yadi.sk/d/ULaMtsQO3NPLjY)

- **[ProfileSchool] Adobe Lightroom: Практика обработки фотографий [2016]**
[Скачать](https://cloud.mail.ru/public/KttT/nczmW3hd5)


### Presets for Lightroom CC
- [VK group for desktop version presets](https://vk.com/ilovelightroom)

- [Tutorial — How to Install on Mobile](https://vk.com/@ilovelightroom-kak-dobavit-presety-v-lightroom-cc)
- [Some group with mobile presets](https://vk.com/lightroomcc)

---

### Adobe Photoshop

- **[Profileschool] Ведущий Андрей Журавлев
Photoshop - Curves. Работа с кривыми​**
[Скачать](https://cloud.mail.ru/public/DFWr/vS9zqQEgp) (Пароль: `www.xomyaki.com`)

- **[ProfileSchool] Алексей Шадрин. Управление визуальным объемом и резкостью изображений**
[Официальный сайт](https://www.profileschool.ru/category/graphics/mk_manage_visual_volume_and_sharp_images)
[Скачать](https://yadi.sk/d/GBQN8cundSCka)

- **Adobe Photoshop. Творческая ретушь**
[Официальный сайт](https://www.profileschool.ru/category/graphics/course_creative_retouching)
[Скачать](https://cloud.mail.ru/public/f4d65d523efa/%D1%80%D0%B5%D1%82%D1%83%D1%88%D1%8C)

- **Adobe Photoshop. Продвинутый уровень**
[Официальный сайт](https://www.profileschool.ru/category/graphics/course_photoshop_advanced)
[Скачать](https://cloud.mail.ru/public/Mc9V/hxK9XtCcc)

- **[ProfileSchool] Детали лица: мейкап и ретушь**
[Официальный сайт](https://www.profileschool.ru/category/graphics/mk_face_details_retouching)
[Скачать](https://cloud.mail.ru/public/4fED/DqKAua5WA)

- **Mastering Gradients in Illustrator**
[Официальный сайт](https://www.skillshare.com/classes/Mastering-Gradients-in-Illustrator/1973622251)
[Скачать](https://mega.nz/#F!cSg0FazB!kQw5ewOLLoAV1YEh6SNo6g)

---

### Adobe inDesign

- **[SoftCulture] Adobe inDesign & Autodesk Revit**
[Скачать](https://yadi.sk/d/cu1skM36UinGIA)

---

### SEO

- **Прогнозная аналитика в SEO**
[Официальный сайт](https://ivanov-seo.ru/course)
[Скачать](https://cloud.mail.ru/public/2oE5/TuYEKRkDz)

---

### Adobe After Effects

- **[Udemy] After Effects CC The Complete Motion Graphics Course**
[Официальный сайт](https://www.udemy.com/adobe-after-effects-2017-essential-motion-graphics-training/)
[Скачать](https://mega.nz/#F!ATY0iQLS!GjVMXetZqL78KhfuIKQXyA)

---

### Cinema 4D

- **10 шагов в Cinema 4D за 2 недели**
[Скачать](https://cloud.mail.ru/public/5A7i/JeZ7PNTWo)

- **Hellolux - learn. Cinema 4D Fields In One Day**
[Скачать](https://cloud.mail.ru/public/6xqs/BXH43vPtw)

- **Полноценный видеокурс по Cinema 4D**
[Скачать](https://cloud.mail.ru/public/FBWw/hXZjbhd98)

---

### Cinema 4D & Adobe After Effects

- **Cinema 4D & After Effects. Школа Алексея Брина**
[Программа курса и описание автора](https://telegra.ph/Cinema-4D--AfterEffects-SHkola-Alekseya-Brina-09-22)
[Скачать](https://drive.google.com/drive/folders/18C8r3l72CHMJ6RCeO_ieJYbDCLZV0Vjo?usp=sharing)

---

### Python

- **[Сергей Черненко]  Python для SEO специалиста (2018)**
[Официальный сайт](https://py4seo.com/)
[Скачать](https://cloud.mail.ru/public/8axN/vZ3VMJ7eZ)


---

### Miscellaneous

- **Мастер-класс «Коммерческая фотография. Рецепт успеха фотографа»**
[Официальный сайт](https://www.profileschool.ru/category/photography/mk_dovg_svet)
[Скачать](https://yadi.sk/d/hrrqFyeVfdfsK)

- **[ProfileSchool] Технологии монтажа плюс видео-кодеки и алгоритмы компрессии (2 в 1)**
[Официальный сайт](https://www.profileschool.ru/category/video/mk_editing_workflow)
[Скачать](https://cloud.mail.ru/public/JShf/NUNY97P5J)

- **[Highlights] Основы рисования (2018)**
[Скачать](https://cloud.mail.ru/public/GBsm/x6nn5XeEE)

- **[Convert Monster] 5-дневный интенсив по интернет-маркетингу (2018)**
[Официальный сайт](https://convertmonster.ru/edu/internetmarketing-intensive/materials2)
[Скачать](https://cloud.mail.ru/public/7jsk/Ex1h3Ydp6)

- **[GeekBrains] Дизайн интерфейсов**
[Скачать](https://cloud.mail.ru/public/FdVu/aDWNdY2FX) (Пароль: `"www.cogamesmoney.ru"`)


---

# [Формула Байеса](https://neerc.ifmo.ru/wiki/index.php?title=%D0%A4%D0%BE%D1%80%D0%BC%D1%83%D0%BB%D0%B0_%D0%91%D0%B0%D0%B9%D0%B5%D1%81%D0%B0)

_Надо решить вот эти две задачи._

И прежде чем лезть в ту муть, что я тебе дал, для начал прочитай [эту статью](http://www.mathprofi.ru/formula_polnoj_verojatnosti_formuly_bajesa.html) как вводную, я сейчас это сделаю и начну уже читать ту длинную статью тоже, чтобы тебе помочь, если что, вот и сам тот [длинный документ](http://schegl2g.bget.ru/bayes/YudkowskyBayes.html), после которого Байес становится как пять пальцев.

---

### Определение вероятности заболевания

Пусть событие $A$ наступило в результате осуществления одной из гипотез $B1,B2…B_n$. Как определить вероятность того, что имела место та или иная гипотеза? Вероятность заразиться гриппом $0.01$. После проведения анализа вероятность, что это грипп $0.9$, другая болезнь $0.001$. Событие $A$ истинно, если анализ на грипп положительный, событие $B_1$ отвечает за грипп, $B_2$ отвечает за другую болезнь. Также предположим, что:

$P(B_1)=0.01, P(B_2)=0.99$ — _априорные_ (оцененные до испытания) вероятности.

$P(A|B_1)=0.9, P(A|B_2)=0.001$ — _апостериорные_ (оцененные после испытания) вероятности тех же гипотез, пересчитанные в связи «со вновь открывшимися обстоятельствами» — с учётом того факта, что событие достоверно произошло.

Рассмотрим вероятность гриппа при положительном анализе:

$$
P(B_1|A)=\frac{P(B_1∩A)}{P(A)}=\frac{P(A|B_1)P(B_1)}{P(A|B_1)P(B1)+P(A|B_2)P(B_2)}=\frac{100}{111}
$$

---

### Парадокс теоремы Байеса

При рентгеновском обследовании вероятность обнаружить заболевание $N$ у больного равна $0.95$, вероятность принять здорового человека за больного равна $0.05$. Доля больных по отношению ко всему населению равна $0.01$. Найти вероятность того, что человек здоров, если он был признан больным при обследовании. Предположим, что:

$P(B_1|B)=0.95,$
$P(B_1|A)=0.05,$
$P(B)=0.01,$
$P(A)=0.99$.

Вычислим сначала полную вероятность признания больным: $0.99⋅0.05+0.01⋅0.95=0.059$

Вероятность «здоров» при диагнозе «болен»: $P(A|B_1)=\frac{0.99⋅0.05}{0.99⋅0.05+0.01⋅0.95}=0.839$

Таким образом, $83.9\%$ людей, у которых обследование показало результат «болен», на самом деле здоровые люди. Удивительный результат возникает по причине значительной разницы в долях больных и здоровых. Болезнь $N$ — редкое явление, поэтому и возникает такой парадокс Байеса. При возникновении такого результата лучше всего сделать повторное обследование.

---




---

[Вот те самые известные пресеты](https://mega.nz/#!EKYDzATY!gE6iy1pXdEnhDdERDlEvO-9jgTUR0bSNt-QmBUFMMPI) [из магазина **VSCO**](http://vsco.co/store/film/mp1)

---
![Envato.Elements](https://s15.postimg.cc/irlogzgej/photo_2018-09-12_00-11-22.jpg)
[Скачать шрифты Envato.Elements](https://cloud.mail.ru/public/Jo4d/JubHAApDk) ~ 2,56Гб






## TODO
 - [x] Download tutorials for Adobe After Effect
 - [ ] Find SEO courses
 - [x] Replace TODO section to bottom
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMzODgxNDY2OCwxMjI5MDc5NDU4LC01MD
k0MjYxNDhdfQ==
-->